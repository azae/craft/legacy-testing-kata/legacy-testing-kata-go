package thirdparty

import (
	"fmt"
	"github.com/gen2brain/dlgs"
)

type QuoteBot struct {
	blog string
}

func New(blog string) *QuoteBot {
	return &QuoteBot{blog}
}

func (quoteBot *QuoteBot) Publish(todayPrice float64) {
	fmt.Printf("Sending %.2f for %s\n", todayPrice, quoteBot.blog)
	_, _ = dlgs.Warning("Big fail!", "You've pushed a dummy auction to a real ads platform, the business is upset!")
	fmt.Println("Big fail!")
}
