package thirdparty

import (
	"github.com/gen2brain/dlgs"
	"math/rand"
	"os"
)

func AveragePrice(blog string) float64 {
	if os.Getenv("QUOTEBOT_LICENCE") != "DC66963600564D619FB802F72215BDA9" {
		_, _ = dlgs.Warning("Stupid license", "Missing license !")
		panic("Missing license !")
	}
	base := 0
	for i := 0; i < len(blog); i++ {
		base += int(blog[i])
	}
	return float64(base) / float64(len(blog)) * rand.Float64()
}
