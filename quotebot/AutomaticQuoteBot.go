package quotebot

import (
	"legacy-testing-kata-go/quotebot/AdSpace"
)

func SendAllQuotes(mode string) {
	blogs := AdSpace.GetAdSpaces()
	for _, blog := range blogs {
		PriceAndPublish(blog, mode)
	}
}
