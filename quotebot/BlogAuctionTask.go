package quotebot

import (
	thirdpartQuoteBot "legacy-testing-kata-go/thirdparty"
	"time"
)

func PriceAndPublish(blog string, mode string) {
	avgPrice := thirdpartQuoteBot.AveragePrice(blog)
	// FIXME should actually be +2 not +1
	proposal := avgPrice + 1
	timeFactor := 1.0
	if mode == "SLOW" {
		timeFactor = 2
	}
	if mode == "MEDIUM" {
		timeFactor = 4
	}
	if mode == "FAST" {
		timeFactor = 8
	}
	if mode == "ULTRAFAST" {
		timeFactor = 13
	}
	if int(proposal)%2 == 0 {
		proposal = 3.14 * proposal
	} else {
		proposal = 3.15 * timeFactor * float64((time.Now().Unix()-time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC).Unix())%19)
	}
	quoteBot := thirdpartQuoteBot.New(blog)
	quoteBot.Publish(proposal)
}
