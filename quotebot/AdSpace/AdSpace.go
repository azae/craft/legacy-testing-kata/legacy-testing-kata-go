package AdSpace

import (
	"legacy-testing-kata-go/quotebot/TechBlogs"
)

var shareCache = map[string][]string{"key": {"value"}}

func GetAdSpaces() []string {
	if val, ok := shareCache["blogs list"]; ok {
		return val
	}

	// FIXME : only return blogs that start with a 'T'
	var allBlogs = TechBlogs.ListAllBlogs()
	shareCache["blogs list"] = allBlogs
	return allBlogs
}
